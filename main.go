// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"sync"

	log "github.com/sirupsen/logrus"

	"codeberg.org/bergblau/webmilterd/internal/config"
	"codeberg.org/bergblau/webmilterd/internal/frontend"
)

func main() {
	c := config.GetConfig()

	// Configure logging
	logLevel, err := log.ParseLevel(c.Log.Level)
	if err != nil {
		log.Errorf("Unknown log level: %s", err)
	} else {
		log.SetLevel(logLevel)
		log.Debugf("Log level set to: %s", c.Log.Level)
	}

	var wg sync.WaitGroup
	
	// Run milter server if configured
	if c.Listen.Milter.Enabled {
		wg.Add(1)

		listener := c.Listen.Milter.Listener()
		defer listener.Close()
		
		server := frontend.NewMilterServer()
		go func() {
			defer wg.Done()
			server.Serve(listener)
		}()
	}

	// Run socketmap server if configured
	if c.Listen.Socketmap.Enabled {
		wg.Add(1)

		listener := c.Listen.Socketmap.Listener()
		defer listener.Close()

		server := frontend.NewSocketmapServer()
		go func() {
			defer wg.Done()
			server.Serve(listener)
		}()
	}

	wg.Wait()
}

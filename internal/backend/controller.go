// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// FIXME fix go generate
//go:generate oapi-codegen -o openapi.gen.go -config oapi-codegen.yaml -package backend spec/docs/concepts/webmilter/openapi.yaml
package backend

import (
	"context"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"

	"codeberg.org/bergblau/webmilterd/internal/config"
)

func IsDomainConfigured(domain string) bool {
	c := config.GetConfig()
	_, ok := c.Domains[domain]
	return ok
}
	
func GetClientByDomain(domain string) (*ClientWithResponses, error) {
	log.Debugf("Getting client for domain %s", domain)

	c := config.GetConfig()
	d, ok := c.Domains[domain]
	if ! ok {
		return nil, fmt.Errorf("Domain is not configured")
	}

	authOpt := WithRequestEditorFn(
		func(ctx context.Context, req *http.Request) error {
			req.SetBasicAuth(d.ClientID, d.ClientSecret)
			return nil
		},
	)
				
	client, err := NewClientWithResponses(d.Server, authOpt)
	if err != nil {
		return nil, err
	}
	return client, nil
}

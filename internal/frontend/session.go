// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"context"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/rs/xid"

	"codeberg.org/bergblau/webmilterd/internal/backend"
)

type session struct {
	sessionId string
}

type SessionError struct {
	Code uint8
	Message string
}

const (
	ErrBareRecipient uint8 = 1
	ErrUnconfDomain  uint8 = 2
	ErrNotFound      uint8 = 3
	ErrBackendFail   uint8 = 4
)

func (e *SessionError) Error() string {
	return fmt.Sprintf("%d - %s", e.Code, e.Message)
}

func newSession() *session {
	s := &session{}
	s.sessionId = xid.New().String()

	return s
}

func (s *session) resolveAlias(address string) ([]string, error) {
	log.Debugf("%s: Resolving alias: %s", s.sessionId, address)

	// Split mail address into local part and domain
	parts := strings.SplitN(address, "@", 2)
	if len(parts) != 2 {
		log.Debugf("%s: Got bare local part as recipient, doing nothing", s.sessionId)
		return []string{}, &SessionError{Code: ErrBareRecipient, Message: "Got a bare recipient"}
	}
	localPart, domain := parts[0], parts[1]

	// Get WebMilter API client
	if ! backend.IsDomainConfigured(domain) {
		log.Debugf("%s: Domain %s not configured, doing nothing", s.sessionId, domain)
		return []string{}, &SessionError{Code: ErrUnconfDomain, Message: "Domain not configured"}
	}
	log.Infof("%s: Handling mail for domain %s", s.sessionId, domain)
	client, err := backend.GetClientByDomain(domain)
	if err != nil {
		log.Errorf("%s: Error getting client for domain %s: %s", s.sessionId, domain, err)
		return nil, err
	}

	// Resolve alias for address
	ctx := context.TODO()
	res, err := client.ResolveAliasByDomainAndLocalpartWithResponse(ctx, domain, localPart)
	if err != nil {
		log.Errorf("%s: Could not resolve aliases against WebMilter API backend: %s", s.sessionId, err)
		return nil, &SessionError{Code: ErrBackendFail, Message: "Could not resolve alias against WebMilter API backend"}
	}
	switch res.StatusCode() {
	case 200:
		// Success, return the list
		return *res.JSON200, nil
	case 404:
		log.Warnf("%s: Alias %s not found in WebMilter API backend", s.sessionId, address)
		return []string{}, &SessionError{Code: ErrNotFound, Message: "Alias not found"}
	default:
		// All other errors mean the API or config is broken, so we communicate a temporary failure
		log.Errorf("%s: Could not resolve aliases against WebMilter API backend: %s", s.sessionId, err)
		return nil, &SessionError{Code: ErrBackendFail, Message: "Could not resolve alias against WebMilter API backend"}
	}
}

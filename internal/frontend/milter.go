// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	milter "github.com/emersion/go-milter"
	log "github.com/sirupsen/logrus"
)

type WebMilter struct {
	milter.NoOpMilter
	session *session
}

func newMilter() milter.Milter {
	m := &WebMilter{}
	m.session = newSession()

	log.Debugf("%s: New milter created", m.session.sessionId)
	return m
}

func (m *WebMilter) RcptTo(address string, mod *milter.Modifier) (milter.Response, error) {
	log.Debugf("%s: Handling RCPT TO: %s", m.session.sessionId, address)

	// Do alias resolution
	targets, err := m.session.resolveAlias(address)
	if err != nil {
		serr, ok := err.(*SessionError)
		if ok {
			// This is a session error, handle it accordingly
			switch serr.Code {
			case ErrBareRecipient, ErrUnconfDomain:
				return milter.RespContinue, nil
			case ErrNotFound:
				return milter.RespReject, nil
			case ErrBackendFail:
				return milter.RespTempFail, nil
			default:
				return milter.RespTempFail, nil
			}
		}
		// We got another, unexpected error
		return milter.RespTempFail, nil
	}

	// Add any recipients we resolved into
	for _, target := range targets {
		log.Debugf("%s: Adding recipient: %s", m.session.sessionId, target)
		mod.AddRecipient(target)
	}
	// Delete virtual recipient we just resolved
	log.Debugf("%s: Removing recipient: %s", m.session.sessionId, address)
	mod.DeleteRecipient(address)

	// Done, go on
	return milter.RespContinue, nil
}

func NewMilterServer() milter.Server {
	server := milter.Server{
		NewMilter: newMilter,
		Actions: milter.OptAddRcpt | milter.OptRemoveRcpt,
		Protocol: milter.OptNoConnect | milter.OptNoHelo | milter.OptNoMailFrom | milter.OptNoBody | milter.OptNoHeaders | milter.OptNoEOH | milter.OptNoUnknown | milter.OptNoData,
	}
	return server
}

// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package frontend

import (
	"strings"

	log "github.com/sirupsen/logrus"
	
	socketmap "codeberg.org/bergblau/postfix-socketmap-table"
)

func lookupAlias(address string) (*socketmap.SocketMapResult, error) {
	session := newSession()
	log.Debugf("%s: Handling socketmap alias lookup: %s", session.sessionId, address)

	// Do alias resolution
	targets, err := session.resolveAlias(address)
	if err != nil {
		serr, ok := err.(*SessionError)
		if ok {
			// This is a session error, handle it accordingly
			switch serr.Code {
			case ErrBareRecipient, ErrUnconfDomain, ErrNotFound:
				return socketmap.NotFound(), nil
			case ErrBackendFail:
				return socketmap.TempFail(serr.Error()), nil
			default:
				return socketmap.TempFail(serr.Error()), nil
			}
		}
		// We got another, unexpected error
		return socketmap.TempFail(err.Error()), nil 
	}

	// Done, return results
	return socketmap.Ok(strings.Join(targets, " ")), nil
}

func NewSocketmapServer() *socketmap.SocketMapServer {
	server := socketmap.NewSocketMapServer()

	server.RegisterMap("aliases", lookupAlias)

	return server
}

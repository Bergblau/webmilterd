// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config

import (
	"net"
	"os"

	log "github.com/sirupsen/logrus"
)

func makeUnixListener(l ListenerConfig) (net.Listener, error) {
	// Try cleaning up existing socket
	if err := os.RemoveAll(l.Address); err != nil {
		log.Warn("Could not clean up listening socket")
	}

	// Start listening to create socket
	listener, err := net.Listen(l.Network, l.Address)
	if err != nil {
		log.Fatalf("Error listening on socket: %s", err)
	}

	// Try to change socket ownership and mode
	if l.User != "" {
		uid, err := l.Uid()
		if err != nil {
			log.Fatalf("Could not resolve user to uid: %s", l.User)
		}
		if err := os.Chown(l.Address, uid, -1); err != nil {
			log.Fatalf("Could not chown socket to user %s: %s", l.User, err)
		}
	}
	if l.Group != "" {
		gid, err := l.Gid()
		if err != nil {
			log.Fatalf("Could not resolve group to gid: %s", l.Group)
		}
		if err := os.Chown(l.Address, -1, gid); err != nil {
			log.Fatalf("Could not chown socket to group %s: %s", l.Group, err)
		}
	}
	if err := os.Chmod(l.Address, l.Mode); err != nil {
		log.Fatalf("Could not chmod socket to mode %s: %s", l.Mode, err)
	}

	return listener, nil
}

func (l ListenerConfig) Listener() net.Listener {
	var listener net.Listener
	var err error
	switch l.Network {
	case "unix":
		listener, err = makeUnixListener(l)
	case "tcp":
		listener, err = net.Listen(l.Network, l.Address)
	default:
		log.Fatalf("Unknown network: %s", l.Network)
	}
	if err != nil {
		log.Fatalf("Could not listen on %s://%s: %s", l.Network, l.Address, err)
	}
	log.Infof("Listening on %s://%s", l.Network, l.Address)

	return listener
}

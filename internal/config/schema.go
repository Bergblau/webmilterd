// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config

import (
	"os"
	"os/user"
	"strconv"
)

type ConfigRoot struct {
	Listen ListenMap
	Log LogConfig
	Domains DomainMap
}

type ListenMap struct {
	Milter ListenerConfig
	Socketmap ListenerConfig
}

type ListenerConfig struct {
	Enabled bool
	Network string
	Address string
	User string
	Group string
	Mode os.FileMode
}

func (l ListenerConfig) Uid() (int, error) {
	user, err := user.Lookup(l.User)
	if err == nil {
		return strconv.Atoi(user.Uid)
	} else {
		return -1, err
	}
}

func (l ListenerConfig) Gid() (int, error) {
	group, err := user.LookupGroup(l.Group)
	if err == nil {
		return strconv.Atoi(group.Gid)
	} else {
		return -1, err
	}
}

type LogConfig struct {
	Level string
}

type DomainMap map[string]DomainConfig

type DomainConfig struct {
	Server string
	ClientID string
	ClientSecret string
}

func buildDefaultConfig() *ConfigRoot {
	return &ConfigRoot{
		Listen: ListenMap{
			Milter: ListenerConfig{
				Enabled: true,
				Network: "unix",
				Address: "/var/spool/postfix/private/webmilterd-milter",
				User: "postfix",
				Group: "postfix",
				Mode: 0660,
			},
			Socketmap: ListenerConfig{
				Enabled: true,
				Network: "unix",
				Address: "/var/spool/postfix/private/webmilterd-socketmap",
				User: "postfix",
				Group: "postfix",
				Mode: 0660,
			},
		},
		Log: LogConfig{
			Level: "warning",
		},
		Domains: DomainMap{},
	}
}

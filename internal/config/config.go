// Copyright 2022 Dominik George <nik@velocitux.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package config

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var config *ConfigRoot

func GetConfig() *ConfigRoot {
	if config != nil {
		return config
	}

	v := viper.NewWithOptions(viper.KeyDelimiter("|"))

	// Load configuration
	v.SetConfigName("webmilterd")
	v.AddConfigPath("/etc/webmilter")
	if err := v.ReadInConfig(); err != nil {
		log.Errorf("Error reading config file: %s", err)
	}

	// Unmarshal into our config structure
	config = buildDefaultConfig()
	err := v.Unmarshal(config)
	if err != nil {
		log.Fatalf("Could not parse config file: %s", err)
	}

	return config
}
